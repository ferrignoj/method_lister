import sublime
import sublime_plugin
import os
from collections import OrderedDict

none = ""


def aaa():
    pass


class ListMethodsCommand(sublime_plugin.TextCommand):
    PREFIX = "Methods: "
    FILE_LINE = "IN_FILE"
    PROJECT_LINE = "IN_PROJECT"
    REUSE_S = 'reuse_views'
    PREFIX_S = 'prefix_views'
    PROJECT_S = "project_prefix"
    FILELINE_S = "filename_prefix"

    def run(self, edit):
        self.settings = sublime.load_settings("MethodsLister.sublime-settings")
        self.PREFIX = self.settings.get(self.PREFIX_S) or self.PREFIX
        self.PROJECT_LINE = self.settings.get(
            self.PROJECT_S) or self.PROJECT_LINE
        self.FILE_LINE = self.settings.get(self.FILELINE_S) or self.FILE_LINE
        self.edit = edit

        self.create_new_tab(self.view.file_name(),
                            self.get_simbols(self.view))

    def get_simbols(self, view):
        # Try to use Ctags modified to load the symbols
        view.run_command('list_symbols')
        if self.settings.get('has_ctags') and view.settings().has('tags_to_list'):
            tag_listss = view.settings().get('tags_to_list')
            # try:
            symbols = self.parse_ctag(tag_listss)
            if len(symbols) != 0:
                return symbols
            # except Exception as e:
            #     print(e.message())
        # if not ctag is present or the previous fails fall back to sublime default
        return self.full_names(self.view, self.view.symbols())

    def parse_ctag(self, tag_listss):
        """
        dict format
        others
        variables
        methods
        classes:
            heading
            others
            variables
            methods
            subclasses

        """
        translator = {
            'd': 'defines',
            't': 'defines',
            'v': 'variables',
            'm': 'methods',
            'f': 'methods',
            'c': 'others'
        }

        results_symbols = []
        tags_dict = OrderedDict()
        tags_dict['defines'] = []
        tags_dict['variables'] = []
        tags_dict['methods'] = []
        tags_dict['others'] = []
        TO_APPEND = 'ex_command'

        # f = open('/tmp/debug', 'w')
        # f.write("-"* 40+ "\n")
        increase_spacing = self.settings.get('increase_spacing')
        for tag_lists in tag_listss:
            prev_type = None
            for tagdict in tag_lists:
                if(not isinstance(tagdict, dict)):
                    continue
                if tagdict['type'] not in translator:
                    prev_type = prev_type or translator['d']
                else:
                    prev_type = prev_type or translator[tagdict['type']]
                # for k, v in tagdict.items():
                #     f.write("%s %s %s\n"%(k,":", v))
                # f.write('-'* 20 + "\n")
                try:
                    current_type = translator[tagdict['type']]
                except:
                    pass
                if not tagdict['fields'] and tagdict['type'] != 'c':
                    if increase_spacing and current_type != prev_type:
                        tags_dict[prev_type].append("")

                    if tagdict['type'] == 'd':
                        tags_dict[current_type].append(
                            "%s %s" % ("#define", tagdict['symbol']))
                    elif tagdict['type'] == 'v' or tagdict['type'] == 'f':
                        tags_dict[current_type].append(tagdict[TO_APPEND])
                else:
                    if increase_spacing and current_type != prev_type:
                        tags_dict['others'].append("")

                    if tagdict['type'] == 'd':
                        tags_dict['defines'].append(
                            "%s %s" % ("#define", tagdict['symbol']))
                    elif tagdict['type'] == 't':
                        tags_dict['defines'].append(tagdict[TO_APPEND])
                    else:
                        if(tagdict['type'] == 'c'):
                            tags_dict['others'].append("")
                        tags_dict['others'].append(tagdict[TO_APPEND])
                prev_type = current_type

        for k, v in tags_dict.items():
            if len(v) > 0:
                results_symbols += v
                results_symbols.append("")

        # f.close()

        return results_symbols

    def you_fuul_look_at_the_data_structure(self):
         # if tagdict['type'] == 'c':
                #         tags_dict['classes'][tagdict['symbol']] = OrderedDict()
                #         tags_dict['classes'][tagdict['symbol']]['heading'] = [tagdict[TO_APPEND]]
                #         tags_dict['classes'][tagdict['symbol']]['others'] = []
                #         tags_dict['classes'][tagdict['symbol']]['variables'] = []
                #         tags_dict['classes'][tagdict['symbol']]['methods'] = []

            # for tagdict in tag_lists:
            #     if(not isinstance(tagdict, dict)):
            #         continue
            #     # First level functions
            #     if not tagdict['fields']:
            #         node = Node(tagdict[TO_APPEND])
            #         if tagdict['type'] == 'v':
            #             tags_dict['variables'].append(node)
            #         elif tagdict['type'] == 'f':
            #             tags_dict['methods'].append(node)
            #         elif tagdict['type'] == 'c':
            #             tags_dict['classes'].append(node)

            #     elif tagdict['type'] == 'd':
            #         tags_dict['others'].append(Node("%s %s" % ("#define", tagdict['symbol'])))
            #     elif tagdict['type'] == 't':
            #         tags_dict['others'].append(Node(tagdict[TO_APPEND]))
                # else:
                #     filed_key = tagdict['field_keys'][0]
                #     if tagdict['type'] == 'v':
                #         tags_dict['variables'].append(node(tagdict[TO_APPEND]))
                #     elif tagdict['type'] == 'f':
                #         tags_dict['methods'].append(node(tagdict[TO_APPEND]))
                #     elif tagdict['type'] == 'c':
                #         tagdict['classes'].append(node(tagdict[TO_APPEND]))

                #     # whell something strange appened
                #     if 'class' in tagdict.keys() and tagdict['class'] not in tags_dict['classes']:

                #         tags_dict['classes'][tagdict['class']] = OrderedDict()
                #         tags_dict['classes'][tagdict['class']]['heading'] = [tagdict['class']]
                #         tags_dict['classes'][tagdict['class']]['others'] = []
                #         tags_dict['classes'][tagdict['class']]['variables'] = []
                #         tags_dict['classes'][tagdict['class']]['methods'] = []

                #     if tagdict['type'] == 'm':
                #         tags_dict['classes'][tagdict['class']]['methods'].append(tagdict[TO_APPEND])
                #     elif tagdict['type'] == 'v':
                #         tags_dict['classes'][tagdict['class']]['variables'].append(tagdict[TO_APPEND])
                #     else:
                #         try:
                #             tags_dict['classes'][tagdict['class']]['others'].append(tagdict['symbol'])
                #         except:
                #             print(tagdict)

        # for k, v in tags_dict.items():
        #     if len(v) > 0:
        #         results_symbols += (n.value for n in v)
        #         results_symbols.append("")

        # # for _, classdict in tags_dict['classes'].items():
        # #     results_symbols.append('')
        # #     for k,v in classdict.items():
        # #         results_symbols += v
        pass

    def full_names(self, view, symbols):
        full_names_symbols = []
        for symbol in symbols:
            # if the line seems to short try to get the wole line
            region = symbol[0]
            if len(symbol[1]) >= abs(region.b - region.a):
                region = view.line(region)
            full_names_symbols.append(view.substr(region))
        return full_names_symbols

    def create_new_tab(self, full_path, symbols):
        name = self.view_name(full_path)
        current_window = self.view.window()
        results_view = self.create_view(current_window, full_path)

        results_view.set_name(name)
        results_view.set_scratch(True)
        results_view.settings().set('word_wrap',
                                    self.view.settings().get('word_wrap'))

        if self.settings.get('show_line_with_project'):
            pp = self.settings.get("untitled_project_name")
            if 'project_path' in self.view.window().extract_variables():
                pp = current_window.extract_variables()['project_path']
            project_line = "%s \"%s\"\n" % (self.PROJECT_LINE, pp)
            self.append_to_view(results_view, project_line)

        if self.settings.get('show_line_with_path'):
            # add the full_path
            file_line = "%s \"%s\"\n\n\n" % (self.FILE_LINE, full_path)
            self.append_to_view(results_view, file_line)

        for symbol in symbols:
            self.append_to_view(results_view, symbol + "\n")

        if self.settings.get('inherit_sintax'):
            results_view.set_syntax_file(self.view.settings().get('syntax'))

        if self.settings.get('transfer_focus_on_view'):
            self.view.window().focus_view(results_view)

        self.run_commands_on_view(current_window, results_view)
        self.set_view_settings(results_view, full_path)

    def run_commands_on_view(self, window, view):
        for command in self.settings.get('command_on_window'):
            window.run_command(command)
        for command in self.settings.get('command_on_view'):
            view.run_command(command)

    def append_to_view(self, view, text):
        view.run_command('append',
                         {'characters': text,
                          'force': True, 'scroll_to_end': False})

    def set_view_settings(self, view, file_path):
        settings = view.settings()
        settings.set("isMethodList", True)
        settings.set("originalFilePath", file_path)
        settings.set("line_numbers", self.settings.get("show_line_numbers"))
        settings.set("gutter", self.settings.get("show_gutter"))

    def view_name(self, full_path):
        title = ""
        if self.settings.get('add_file_name_to_title'):
            if full_path:
                filename = full_path.split(os.path.sep)[-1]
            else:
                filename = self.settings.get('untitled_filename')

            title = "{}: {}".format(self.PREFIX, filename)
        else:
            title = self.PREFIX
        return title

    def create_view(self, window, full_path):
        if self.settings.get(self.REUSE_S):
            for view in window.views():
                if view.settings().has("isMethodList") and \
                   view.name().startswith(self.PREFIX):
                    try:
                        saved_path = view.settings().get('originalFilePath')
                        if saved_path == full_path:
                            # clear the view
                            view.erase(self.edit,
                                       sublime.Region(0, view.size()))
                            return view
                    except IndexError:
                        pass
        return window.new_file()


class GotoClassFileCommand(sublime_plugin.ViewEventListener):

    @classmethod
    def is_applicable(self, settings):
        return settings.has("isMethodList")

    def on_selection_modified(self):
        self.run()

    def run(self):
        self.settings = sublime.load_settings("MethodsLister.sublime-settings")
        cview = self.view
        current_line = cview.substr(cview.line((cview.sel()[0]))).strip()
        file_path = self.get_file_path()
        if not file_path:
            return
        origin_view = self.get_origin_view(file_path)
        # find the symbols
        destination_region = origin_view.find(current_line,
                                              origin_view.text_point(1, 1),
                                              sublime.LITERAL)
        if destination_region.a == -1 and destination_region.b == -1:
            return

        origin_view.sel().clear()
        # place the cursor to the beginning of the line
        if not self.settings.get("select_simbol"):
            destination_region = sublime.Region(destination_region.a,
                                                destination_region.a)
        origin_view.sel().add(destination_region)
        origin_view.show(destination_region)
        if not self.settings.get('show_cursor'):
            origin_view.sel().clear()

    def get_file_path(self):
        if self.view.settings().has("originalFilePath"):
            return self.view.settings().get("originalFilePath")
        return None

    def get_origin_view(self, path):
        window = self.view.window()
        origin_view = window.open_file(path)
        return origin_view
